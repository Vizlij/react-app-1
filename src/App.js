import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import AppRoutes from './components/routes/routes.component';

const App = () => {
    return <AppRoutes />
};
export default App