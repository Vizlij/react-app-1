import React from 'react'
import Game from "../game/game.component";
import Home from "../Home/Home";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import AboutPage from "../AboutPage/AboutPage";
import LogsPage from "../LogsPage/LogsPage"

export default class AppRoutes extends React.PureComponent {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path='/' component={Home} />
                    <Route exact path='/game' component={Game} />
                    <Route path='/google' href="google.com"/>
                    <Route path='/about' component={AboutPage}/>
                    <Route path='/logs' component={LogsPage}/>
                </Switch>
            </BrowserRouter>
        )
    }
}