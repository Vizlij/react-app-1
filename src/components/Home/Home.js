import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import HomeHeader from "./components/HomeHeader/HomeHeader";
// import './HomeStyle.css'
import PageHeader from "./components/PageHeader/PageHeader";
import PageContents from "./components/PageContents/PageContents";
import FooterPage from "./components/FooterPage/FooterPage";

class Home extends React.Component {

    render() {
        return (
            <div className="home">
                <HomeHeader />
                <PageHeader />
                <PageContents />
                <FooterPage />
            </div>
        )
    };
}
export default Home
