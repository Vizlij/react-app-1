import React from 'react';
import {Container, Nav, Navbar} from "react-bootstrap";

export default class HomeHeader extends React.Component {
    render() {
        return (
                <Navbar variant="dark" bg="dark">
                    <Container>
                        <Navbar.Brand href="https://google.com">Go to Google!</Navbar.Brand>
                        {/*<Button className="btn navbar-toggler">...</Button>*/}
                        <Nav className="ml-auto">
                            <Nav.Link href="/">Home</Nav.Link>
                            <Nav.Link href="/game">Game</Nav.Link>
                            <Nav.Link href="/about">About</Nav.Link>
                            <Nav.Link href="/logs">Logs</Nav.Link>
                        </Nav>
                    </Container>
                </Navbar>
        )
    }
}