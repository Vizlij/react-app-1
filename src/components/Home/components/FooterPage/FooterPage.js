import React from 'react'
import {Card, Container} from "react-bootstrap";

export default class FooterPage extends React.Component {
    render() {
        return (
            <Card className="py-5 bg-dark">
                <Container>
                    <p className="m-0 text-center text-white">
                        Copyright &copy; Your Website 2019
                    </p>
                </Container>
            </Card>
        )
    }
}