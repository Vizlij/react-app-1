import React from 'react';
import {Card, Col, Container, Row} from "react-bootstrap";

export default class PageContents extends React.Component {
    state = {
        data: []
    };

    componentDidMount() {
        const response = [
            {
                id: 1,
                title: 'Cart Title',
                content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet ducimus ea eaque fugit illum incidunt iste libero modi!'
            },{
                id: 2,
                title: 'Cart Title',
                content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet ducimus ea eaque fugit illum incidunt iste libero modi!'
            },{
                id: 3,
                title: 'Cart Title',
                content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet ducimus ea eaque fugit illum incidunt iste libero modi!'
            },{
                id: 4,
                title: 'Cart Title',
                content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet ducimus ea eaque fugit illum incidunt iste libero modi!'
            },
        ];

        this.setState({
            data: response
        })
    }
    render() {
        return (
            <Container>
                <Row className="text-center">
                    {
                        this.state.data.map((val, key) => (
                            <Col className="col-lg-3 col-md-6 mb-4" key={`card-${val.id}`}>
                                <Card>
                                    <Card.Img variant="top" src="http://placehold.it/500x325"/>
                                    <Card.Body>
                                        <Card.Title className="h4">{val.title}</Card.Title>
                                        <Card.Text>
                                            {val.content}
                                        </Card.Text>
                                    </Card.Body>
                                    <Card.Footer>
                                        <a href="#" className="btn btn-dark">Find Out More!</a>
                                    </Card.Footer>
                                </Card>
                            </Col>
                        ) )
                    }
                </Row>

            </Container>
        )
    }
}