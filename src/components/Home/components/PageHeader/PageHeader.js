import React from 'react';
import {NavLink} from "react-router-dom";
import {Card, Container} from "react-bootstrap";

export default class PageHeader extends React.Component {
    render() {
        return (
            <Container>
                <Card className="jumbotron my-4">
                    <Card.Body>
                        <Card.Title className="display-3">Welcome you to my game!</Card.Title>
                        <Card.Text className="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Aliquid aperiam at corporis, deleniti ea earum perspiciatis placeat
                            quaerat quam quis quisquam repellendus vel vero? Dicta necessitatibus
                            officia rem tempore unde.
                        </Card.Text>
                        <hr/>
                        <NavLink className="btn btn-dark btn-lg" to="/game">Go to Game!</NavLink>
                    </Card.Body>
                </Card>
            </Container>
        )
    }

}