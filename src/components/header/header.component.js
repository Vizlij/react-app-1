import React from 'react';
import {Navbar, Button, Container} from 'react-bootstrap';
import {NavLink} from "react-router-dom";

export default class Header extends React.Component {

    render () {
        return (
            <Navbar variant="dark" bg="dark">
                <Container>
                        <NavLink className="btn btn-outline-dark btn-sm active" to="/">
                            Home
                        </NavLink>
                        <Button className="btn btn-outline-dark btn-lg active"
                                onClick={() => this.props.startGame()}>Start
                        </Button>
                        <Button className="btn btn-outline-dark btn-sm active"
                                onClick={() => {this.props.handleShow(); this.props.startGame();}}>Exit
                        </Button>
                </Container>
            </Navbar>
        )
    }
}
