import React from "react"
import './logs.css'
import arrow from '../images/back-arrow.png'


export default class Logs extends React.Component {

    render() {
        const history = this.props.history;

        const logs = history.map((step, move) => {
            if(move === 0)
                    return;

            const desk = move ?
                'Go to move #' + move :
                'Go to game start';
            return (

                    <div key={move}>
                        {desk}
                        <img src={arrow} onClick={() => this.props.jumpTo(move)} className="iconArr"/>
                    </div>
            );
        });
        return <div className="footer">{logs}</div>;
    }
}