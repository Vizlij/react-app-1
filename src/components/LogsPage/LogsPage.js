import React from 'react'
import HomeHeader from "../Home/components/HomeHeader/HomeHeader";
import LogsContents from "./LogsContents/LogsContents";

export default class LogsPage extends React.Component {
    render() {
        return (
            <div className="logsPage">
                <HomeHeader />
                <LogsContents />
            </div>
        )
    }
}