import React from 'react'
import {Card, Container, Row} from "react-bootstrap";
import './LogsContents.css'

export default class LogsContents extends React.Component {
    render() {
        return (
            <Container>
                <Row>
                    <Card className="jumbotron my-4 bg-warning col-lg-12">
                        <Card.Title className="display-4">
                            There will be logs.
                        </Card.Title>
                    </Card>
                    <Card className="col-lg-12 bg-warning">
                        <Card.Body className="logOutput">

                        </Card.Body>
                    </Card>
                </Row>
            </Container>
        )
    }
}