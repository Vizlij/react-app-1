import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import {NavLink} from "react-router-dom";

export default class ModalWindow extends React.Component {
    // constructor(props) {
    //     super(props);
    // }

    render() {
        console.log(this.props);
        return (
                <Modal show={this.props.show} onHide={this.props.handleClose}>
                    <Modal.Body className="h4">Thanks for playing</Modal.Body>
                    <Modal.Footer>
                        <Button className="btn btn-dark btn-sm active mr-auto" onClick={this.props.handleClose}>
                            Play more
                        </Button>
                        <NavLink className="btn btn-dark btn-sm active" to="/">Go Home!</NavLink>
                        <NavLink className="btn btn-dark btn-sm active" to="/about">About</NavLink>
                    </Modal.Footer>
                </Modal>
        );
    }
}

