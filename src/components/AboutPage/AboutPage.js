import React from 'react'
import HomeHeader from '../Home/components/HomeHeader/HomeHeader'
import AboutContents from './AboutContents'
import FooterPage from '../Home/components/FooterPage/FooterPage'

export default class AboutPage extends React.Component {
    render() {
        return (
            <div>
                <HomeHeader />
                <AboutContents />
                <FooterPage/>
            </div>
        )
    };
}
