import React from 'react'
import myFace from '../images/myFace.jpg'
import {Card, Col, Container, Row} from "react-bootstrap";

export default class AboutContents extends React.Component {
    render() {
        return (
            <Container>
                <Row>
                    <Col className="col-md-6">
                        {/*<Card.Img src={myFace} className="rounded-circle col-md-3 mt-4"/>*/}
                        <Card className="my-4 ml-4">
                            <Card.Body>
                                <Card.Img src={myFace} className="rounded-circle col-md-3"/>
                                <Card.Title className="display-5">Information about me</Card.Title>
                                <Card.Text className="lead">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing
                                    elit. Aliquam aperiam asperiores, cupiditate dolor
                                    doloremque eius ex expedita facilis labore maiores,
                                    modi nam neque nisi optio sequi sint soluta sunt vero?
                                </Card.Text>
                            </Card.Body>
                    </Card>
                    </Col>
                </Row>
            </Container>
        )
    }
}