import React from "react";
import Board from '../board/board.component'
import {calculateWinner} from "../utils/calculateWiner";
import Header from '../header/header.component';
import Logs from '../logs/logs.component';
import ModalWindow from '../Modal/modal.component';
import './game.style.css'
import { Api } from '../../services/api';

export default class Game extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            history: [{
                squares: Array(9).fill(null),
            }],
            stepNumber: 0,
            xIsNext: true,
            show: false,
        };
    }

    handleClose = () => {
        this.setState({ show: false });
    };

    handleShow = () => {
        this.setState({ show: true });
    };

    startGame =  () => {     // реализация "новой игры"
        this.setState ({
            history: [{
                squares: Array(9).fill(null),
            }],
            stepNumber: 0,
        })
    };

    async handleClick (i) {
        const history = this.state.history.slice(0, this.state.stepNumber + 1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();
        if (calculateWinner(squares) || squares[i]) {
            return;
        }
        squares[i] = this.state.xIsNext ? 'X' : 'O';
        this.setState ({
            history: history.concat([{
                squares: squares,
            }]),
            stepNumber: history.length,
            xIsNext: !this.state.xIsNext
        });

        await Api.logs.add({
            name: squares[i],
            move: i
        })
    }

    // if(this.statet.xIsNext) {
    //     squares[i] = 'X'
    // } else {
    //     squares[i] = 'O'
    // }

    jumpTo = step => {
        this.setState({
            stepNumber: step,
            xIsNext: (step % 2) === 0,
        });
    };

    render() {
        const history = this.state.history;
        const current = history[this.state.stepNumber];
        const  winner = calculateWinner(current.squares);

        let status ;
        if (winner) {
            status = 'Winner: ' + winner;
        } else {
            status = 'Next player ' + (this.state.xIsNext ? 'X' : 'O');
        }

        return (
            <div className="game-style">
                <Header startGame={this.startGame} handleShow={this.handleShow}/>
                <div className="game-info">
                    <div>{status}</div>
                    {/*<ol>{moves}</ol>*/}
                </div>
                <div className="game">
                    <div className="game-board">
                        <Board
                            squares={current.squares}
                            onClick={(i) => this.handleClick(i)}
                        />
                    </div>
                </div>
                <Logs history={history}  jumpTo={this.jumpTo}/>
                <ModalWindow show={this.state.show} handleClose={this.handleClose}/>
            </div>
        );
    }
}