import { LogsApiService } from './logsAPI';

class ApiService {
  constructor() {
    this.logs = new LogsApiService();
  }
}

export const Api = new ApiService();
