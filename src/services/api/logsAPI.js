import BaseApiService from './base';

export class LogsApiService extends BaseApiService {
    // eslint-disable-next-line
    constructor() {
        super();
    }
// data = {
//     name: 'X',
//     method: '4'
//
// }
    add(data) {
        return this.request({
            method: 'post',
            url: '/api/logs/add',
            data
        });
    }
}
